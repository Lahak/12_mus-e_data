<?php

	include('php/formengine.php');

?>
<!DOCTYPE html>
<html>
	<!-- DEBUT HEAD -->
	<head>

		<!-- DÉBUT DES METAS -->
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="description" content="Site sur les musés"/> <!-- DESCRIPTION DU SITE -->
		<meta name="author" content="Benjamain C. - Olivier L. - Morgane D. - Adam M."/> <!-- AUTEURS DU SITE -->
		<meta name="keywords" content="Data, musés, informations, ACS"/> <!-- MOT-CLEFS DU SITE -->
		<!-- FIN DES METAS -->

		<title>Musée de France</title>

		<!-- DÉBUT DE LIAISON DES FICHIERS -->
		<link rel="icon" href="img/favicon.ico"/> <!-- MISE EN PLACE DE NOTRE FAVICON -->
		<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/> <!-- PAGE CSS DE BOOTSTRAP -->
		<link href="css/starter-template.css" rel="stylesheet"/> <!-- PAGE CSS DU TEMPLATE BOOTSTRAP -->
		<link href="css/style.css" rel="stylesheet"/> <!-- PAGE DE NOTRE PROPRE CSS -->
		<!-- FIN DE LIAISON DES FICHIERS -->

	</head>
	<!-- FIN HEAD -->

	<!-- DEBUT BODY -->
	<body class="container-fluid">

		<!-- DÉBUT HEADER -->
		<header>

			<!-- DÉBUT NAV -->
			<nav class="navbar navbar-default">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">Musées de France</a>
				</div>


				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class=""><a href="searchmuseum.php">Chercher un musée <span class="sr-only">(current)</span></a></li>
						<li><a href="index.php">Accueil</a></li>
						<li><a style="display:none;" href="#">Administration</a></li>
					</ul>
				</div>
			</nav>
			<!-- FIN NAV -->

		</header>
		<!-- FIN HEADER -->

		<div class="container-fluid">

			<form class="form-group" action="showmuseum.php" method="POST" onsubmit="searchresult();" >
				<div class="form-control">
					<label for="region">Région : </label>
					<select id ="region" name="region">
						<option value="" selected>--</option>

						<?php

							buildform("region");

							//print_r($data);
						?>

					</select>
				</div>
				<div class="form-control">
					<label for="departement">Département : </label>
					<select id="departement" name="departement">
						<option value="" selected>--</option>

						<?php

							buildform("");

							//  print_r($data);

						?>

					</select>
				</div>

				<div class="form-control">
					<label for="nom_musée">Nom du musée : </label>
					<input type="text" id="nom_musée" name="nom_musée"/>
				</div>
				<input type="submit" class="btn btn-success" value="Rechercher"/>
			</form>


		</div>
		<div class="" id="searchresult">


		</div>

		<!-- DÉBUT DES SCRIPTS -->
		<?php include("php/script.php"); ?>

		<script type="text/javascript"> // SCRIPT DES SELECT EN LIEN

			$("#departement").chained("#region");

		</script>

		
		<!-- FIN DES SCRIPTS -->

	</body>
</html>
