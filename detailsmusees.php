﻿<!DOCTYPE html>
<html lang="fr">
	<!-- DEBUT HEAD -->
	<head>
		
		<!-- DÉBUT DES METAS -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Site sur les musées"/> <!-- DESCRIPTION DU SITE -->
		<meta name="author" content="Benjamain C. - Olivier L. - Morgane D. - Adam M."/> <!-- AUTEURS DU SITE -->
		<meta name="keywords" content="Data, musées, informations, ACS"/> <!-- MOT-CLEFS DU SITE -->
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<!-- Elément Google Maps indiquant que la carte doit être affiché en plein écran et
		qu'elle ne peut pas être redimensionnée par l'utilisateur -->
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<!-- Inclusion de l'API Google MAPS -->
		<!-- Le paramètre "sensor" indique si cette application utilise détecteur pour déterminer la position de l'utilisateur -->
		
		<!-- FIN DES METAS -->
		
		<title>Musée de France</title>
		
		<!-- DéBUT DE LIAISON DES FICHIERS -->
		<link rel="icon" href="img/favicon.ico"/> <!-- MISE EN PLACE DE NOTRE FAVICON -->
		<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/> <!-- PAGE CSS DE BOOTSTRAP -->
		<link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
		<link href="css/starter-template.css" rel="stylesheet"/> <!-- PAGE CSS DU TEMPLATE BOOTSTRAP -->
		<link href="css/style.css" rel="stylesheet"/> <!-- PAGE DE NOTRE PROPRE CSS -->
		<!-- FIN DE LIAISON DES FICHIERS -->
		
	</head>
	<!-- FIN HEAD -->
	
	<!-- DEBUT BODY -->
	<body class="container-fluid" onload="initialiser()">
		
		<!-- DéBUT HEADER -->
		<header>
			
			<!-- DÉBUT NAV -->
			<nav class="navbar navbar-default">
				
				<div class="navbar-header nav">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				
				
				<div class="collapse navbar-collapse nav" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a class="navbar-brand nav_a" href="index.php"><img src="img/logo.png" alt="Logo du site"/></a></li>
						<li><a class="nav_a" href="index.php">Accueil</a></li>
						<li class=""><a class="nav_a" href="about.php">À propos de nous<span class="sr-only">(current)</span></a></li>
						<li><a style="display:none;" href="#">Administration</a></li>
					</ul>
				</div>
			</nav>
			<!-- FIN NAV -->
			
		</header>
		<!-- FIN HEADER -->
		
		<!-- DéBUT SECTION 1 -->
		<section class="col-xs-12">
			
			<?php 
				
				$id = $_REQUEST['id'];
				
				// echo $id;
				
				include("php/dbconnect.php");
				
				$sql = "SELECT * FROM liste_musees WHERE id = \"".$id."\"";
				
				$template_presmusee = file_get_contents("template/presmusee.html");
				
				// echo $sql;
				
				include("php/dbdriver.php");
				
				// print_r($data);
				
				for ($i=0; $i < sizeof($data) ;$i++) 
				{
					
					$presmusee = $template_presmusee;
					
					foreach ($data[$i] as $key => $value) 
					{	
						$presmusee = str_replace("{{".$key."}}", $value, $presmusee);
					}
					$html.= $presmusee;
				}
				echo $html;
			?>
			
			<div id="carte" style="width:100%; height:450px"></div>
		</section>
		<!-- FIN SECTION 1 -->
		
		<footer class="footer">
			<p>ACS Museum - By Morgane D, Benjamain C, Olivier L, Adam M - Projet d'étude pour l'<a href="http://accesscodeschool.fr">Access Code School</a></p>
		</footer>
		
		<!-- DéBUT DES SCRIPTS -->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
			function initialiser() {
				var latlng = new google.maps.LatLng(<?php echo $data[0]['LAT'].",".$data[0]['LNG'];
				?>);
				//objet contenant des propriétés avec des identificateurs prédéfinis dans Google Maps permettant
				//de définir des options d'affichage de notre carte
				var options = {
					center: latlng,
					zoom: 13,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				
				
				
				//constructeur de la carte qui prend en paramêtre le conteneur HTML
				//dans lequel la carte doit s'afficher et les options
				var carte = new google.maps.Map(document.getElementById("carte"), options);
				
				myMarker = new google.maps.Marker({
					position: latlng,
					map: carte,
					title: "coordonnées"
					
					
				});
			}
			
		</script>
		<?php include("php/script.php"); ?>
		<script src="js/randombackground.js"></script>
		
		<!-- FIN DES SCRIPTS -->
		
	</body>
	<!-- FIN DU BODY -->
	
</html>					