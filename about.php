<!DOCTYPE html>

<html lang="fr">
	
	<!-- DEBUT HEAD -->
	<head>
		
		<!-- DÉBUT DES METAS -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Site sur les musées"/> <!-- DESCRIPTION DU SITE -->
		<meta name="author" content="Benjamain C. - Olivier L. - Morgane D. - Adam M."/> <!-- AUTEURS DU SITE -->
		<meta name="keywords" content="Data, musées, informations, ACS"/> <!-- MOT-CLEFS DU SITE -->
		<!-- FIN DES METAS -->
		
		<title>Musée de France</title>
		
		<!-- DÉBUT DE LIAISON DES FICHIERS -->
		<link rel="icon" href="img/favicon.ico"/> <!-- MISE EN PLACE DE NOTRE FAVICON -->
		<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/> <!-- PAGE CSS DE BOOTSTRAP -->
		<link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
		<link href="css/starter-template.css" rel="stylesheet"/> <!-- PAGE CSS DU TEMPLATE BOOTSTRAP -->
		<link href="css/style.css" rel="stylesheet"/> <!-- PAGE DE NOTRE PROPRE CSS -->
		<!-- FIN DE LIAISON DES FICHIERS -->
		
	</head>
	<!-- FIN HEAD -->
	
	<!-- DEBUT BODY -->
	<body class="container-fluid">
		
		<!-- DÉBUT HEADER -->
		<header>
			
			<!-- DÉBUT CARROUSEL -->
			<div class="filtre">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>
					
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active container-fluid">
							<img src="img/image_carrousel_a.jpg" alt="Image musée déco 1"/>
						</div>
						
						<div class="item">
							<img src="img/image_carrousel_b.jpg" alt="Image musée déco 2"/>
						</div>
						
						<div class="item">
							<img src="img/image_carrousel_c.jpg" alt="Image musée déco 3"/>
						</div>
					</div>
				</div>
			</div>
			<!-- FIN CARROUSEL -->
			
			
			<!-- DÉBUT NAV -->
			<nav class="navbar navbar-default">
				
				<div class="navbar-header nav">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				
				
				<div class="collapse navbar-collapse nav" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a class="navbar-brand nav_a" href="index.php"><img src="img/logo.png" alt="Logo du site"/></a></li>
						<li><a class="nav_a" href="index.php">Accueil</a></li>
						<li class=""><a class="nav_a" href="about.php">À propos de nous<span class="sr-only">(current)</span></a></li>
						<li><a style="display:none;" href="#">Administration</a></li>
					</ul>
				</div>
			</nav>
			<!-- FIN NAV -->
			
		</header>
		<!-- FIN HEADER -->
		
		<h1 class="text-center">À propos de nous</h1>
		
		<!-- DÉBUT SECTION 1 -->
		<section class="container col-xs-12 col-sm-6 col-md-6 col-lg-4 col-lg-offset-1">
			
			
			<h2>Le projet Musée de France </h2>
			<img src="img/imgProjet.svg" alt="Logo Projet" height="100"/>
			<p>
				Le projet ACS Museum est un exercice proposé par l'école <a href="http://www.accesscodeschool.fr/">Access Code School</a> de Besançon à ses étudiants en informatique afin d'établir une approche de l'exploitation de l'open data. L'exercice a été proposé à 16 étudiants, divisé en groupe de 4. Chacun a démarré le projet à partir d'une liste de données ouvertes trouvée sur Data-Gouv : <a href="https://www.data.gouv.fr/fr/datasets/liste-et-localisation-des-musees-de-france/">https://www.data.gouv.fr/fr/datasets/liste-et-localisation-des-musees-de-france/</a> sous format Excel. Le projet n'avait aucune restriction de technologie et avait pour seule condition d'exploiter au maximum et de façon harmonieuse les données sur les musées. À vous de juger si le travail est réussi.
			</p>
			
			
		</section>
		<!-- FIN SECTION 1 -->
		
		<!-- DÉBUT SECTION 2 -->
		<section class="container col-xs-12 col-sm-6 col-md-6 col-lg-4 col-lg-offset-2">
			<h2>Qui sommes-nous ?</h2>
			<img src="img/imgTeam.svg" alt="Logo Projet" height="100"/>
			<p>Ce site à été réalisé par quatre étudiants de l'Access Code School : </p>
			<ul>
				<li class="puce_couleur">Morgane D : Développeuse qui adore les effets tapes à l'oeil et la modernité</li>
				<li class="puce_couleur">Benjamain C : Développeur minutieux accros à l'animation en tout genre</li>
				<li class="puce_couleur">Olivier L : Développeur polyvalent et curieux, toujours disponible et efficace à chaque moment</li>
				<li class="puce_couleur">Adam M : Développeur organisé toujours prêt pour nettoyer le code et les fichiers</li>
			</ul>
			
			
		</section>
		<!-- FIN SECTION 2 -->
		
		<!-- DÉBUT SECTION 3 -->
		<section class="col-xs-12 container">
			
			<!-- DEBUT BOITE -->
			<div class="box_form">
				
				<!-- DEBUT DU FORMULAIRE -->
				<form action="postContact.php" id="contactForm" method="POST" onsubmit="return false;">
					
					<h2> Contact </h2>
					
					<!-- DEBUT BLOCK NOM -->
					<div class="champ_normal">
						<h3>Nom</h3>
						<input type="text" name="nameGuest" placeholder="Entrez votre nom"/>
					</div>
					<!-- FIN BLOCK NOM -->
					
					<!-- DEBUT BLOCK PRENOM -->
					<div class="champ_normal">
						<h3>Prénom</h3>
						<input type="text" name="surnameGuest" placeholder="Entrez votre prénom" />
					</div>
					<!-- FIN BLOCK PRENOM -->
					
					<!-- DEBUT BLOCK MAIL -->
					<div class="champ_normal">
						<h3>* Mail</h3>
						<input type="email" name="mailGuest" placeholder="Entrez votre adresse mail" required />
					</div>
					<!-- FIN BLOCK MAIL -->
					
					<!-- DEBUT BLOCK TEXTAREA -->
					<div class="champ_textarea">
						<h3>* Message </h3>
						<textarea placeholder="Entrez votre message" name="messageGuest" required></textarea>				
					</div>
					<!-- FIN BLOCK TEXTAREA -->
					
					<!-- DEBUT BOUTON ENVOYER -->
					<div class="bouton_envoyer">
						<p>Les champs * sont obligatoires</p>
						<input class="bouton" id="submitContact" type="submit" value="Envoyer"/>
					</div>
					<!-- FIN BOUTON ENVOYER -->
					
				</form>
				<!-- FIN DU FORMULAIRE -->
				
			</div>
			<!-- FIN BOITE -->
			
			
			
		</section>
		<!-- FIN SECTION 3 -->
		<footer class="footer">
			<p>ACS Museum - By Morgane D, Benjamain C, Olivier L, Adam M - Projet d'étude pour l'<a href="http://accesscodeschool.fr">Access Code School</a></p>
		</footer>
		<!-- DÉBUT DES SCRIPTS -->
		<?php include("php/script.php"); ?>
		
		<script>
			$(function() {
				$('#submitContact').click(function() {
					var str = $("#contactForm").serialize();
					// alert(str);
					$.ajax({
						type: 'POST',
						url: 'postContact.php',
						datatype: 'html',
						data: str,
						timeout: 3000,
						success: function(data) {
						alert('Votre message à bien été envoyer.');},
						error: function() {
						alert('La requête n\'a pas abouti'); }
					});    
				});  
			});
		</script>
		<!-- FIN DES SCRIPTS -->
		
	</body>
	<!-- FIN DU BODY -->
	
</html>