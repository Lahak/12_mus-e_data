<?php
	
	include('php/formengine.php');
	
?>
<!DOCTYPE html>
<html>
	<!-- DEBUT HEAD -->
	<head>
		
		<!-- DÉBUT DES METAS -->
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="description" content="Site sur les musées"/> <!-- DESCRIPTION DU SITE -->
		<meta name="author" content="Benjamain C. - Olivier L. - Morgane D. - Adam M."/> <!-- AUTEURS DU SITE -->
		<meta name="keywords" content="Data, musées, informations, ACS"/> <!-- MOT-CLEFS DU SITE -->
		<!-- FIN DES METAS -->
		
		<title>Musée de France</title>
		
		<!-- DÉBUT DE LIAISON DES FICHIERS -->
		<link rel="icon" href="img/favicon.ico"/> <!-- MISE EN PLACE DE NOTRE FAVICON -->
		<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/> <!-- PAGE CSS DE BOOTSTRAP -->
		<link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'/>
		<link href="css/starter-template.css" rel="stylesheet"/> <!-- PAGE CSS DU TEMPLATE BOOTSTRAP -->
		<link href="css/style.css" rel="stylesheet"/> <!-- PAGE DE NOTRE PROPRE CSS -->
		<!-- FIN DE LIAISON DES FICHIERS -->
		
	</head>
	<!-- FIN HEAD -->
	
	<!-- DEBUT BODY -->
	<body class="container-fluid">
		
		<!-- DÉBUT HEADER -->
		<header>
			
			<!-- DÉBUT CARROUSEL -->
			<div class="filtre">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>
					
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active container-fluid">
							<img src="img/image_carrousel_a.jpg" alt="Image musée déco 1"/>
						</div>
						
						<div class="item">
							<img src="img/image_carrousel_b.jpg" alt="Image musée déco 2"/>
						</div>
						
						<div class="item">
							<img src="img/image_carrousel_c.jpg" alt="Image musée déco 3"/>
						</div>
					</div>
				</div>
			</div>
			<!-- FIN CARROUSEL -->
			
			
			<!-- DÉBUT NAV -->
			<nav class="navbar navbar-default">
				
				<div class="navbar-header nav">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				
				
				<div class="collapse navbar-collapse nav" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a class="navbar-brand nav_a" href="index.php"><img src="img/logo.png" alt="Logo du site"/></a></li>
						<li><a class="nav_a" href="index.php">Accueil</a></li>
						<li class=""><a class="nav_a" href="about.php">À propos de nous<span class="sr-only">(current)</span></a></li>
						<li><a style="display:none;" href="#">Administration</a></li>
					</ul>
				</div>
			</nav>
			<!-- FIN NAV -->
			
		</header>
		<!-- FIN HEADER -->
		
		<div class="container-fluid" id="searchEngine">
			<h1 class="text-center">ACS Museum</h1>
			<p class="text-center">Pour trouver les musées d'une région il vous suffit de compléter les filtres suivants dans l'ordre.</p>
			<form class="form-group" id="museumForm" action="showmuseum.php" method="POST" onsubmit="return false;" >
				<div class="text-center">
					<label for="region">Par région : </label>
					<select id ="region" name="region" class="input-small">
						<option value="" selected>Sélectionner une région</option>
						
						<?php
							
							buildform("region");
							
							//print_r($data);
						?>
						
					</select>
					<label for="departement">&emsp;Par département : </label>
					<select id="departement" name="departement" class="input-small">
						<option value="" selected>Sélectionner un département</option>
						
						<?php
							
							buildform("");
							
							//  print_r($data);
							
						?>
						
					</select>
					<!-- <label for="nom_musée">&emsp;Nom du musée : </label>
					<input type="text" id="nom_musée" name="nom_musée" class="input-small"/> -->
					&emsp; <input type="submit" class="btn btn-danger" value="Rechercher" id="submitMuseum"/>
				</div>
			</form>
			
			
		</div>
		
		<div id="searchresult">
			
			
		</div>
		
		<footer class="footer">
			<p>ACS Museum - By Morgane D, Benjamain C, Olivier L, Adam M - Projet d'étude pour l'<a href="http://accesscodeschool.fr">Access Code School</a></p>
		</footer>
		<!-- DÉBUT DES SCRIPTS -->
		<?php include("php/script.php"); ?>
		
		<script type="text/javascript"> // SCRIPT DES SELECT EN LIEN
			
			$("#departement").chained("#region");
			
		</script>
		
		<script>
			$(function() {
				$('#submitMuseum').click(function() {
					var str = $("#museumForm").serialize();
					// alert(str);
					$.ajax({
						type: 'POST',
						url: 'showmuseum.php',
						datatype: 'html',
						data: str,
						timeout: 3000,
						success: function(data) {
							$("#searchresult").html(data);
							location.href="#searchEngine";
						},
						error: function() {
						alert('La requête n\'a pas abouti'); }
					});    
				});  
			});
		</script>
		
		<!-- FIN DES SCRIPTS -->
		
	</body>
</html>